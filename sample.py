import sys
import os
from subprocess import call
import emcee
import pandas as pd
import numpy as np
from scipy import stats

import fastpt as fpt
import fastpt.HT as HT
import pyccl as ccl
import pyccl.ccllib as lib
import pyccl.nl_pt as pt

class sample:
    
    def __init__(self, fname_wgg, fname_cov_wgg, fname_wgp, fname_cov_wgp, suffix, z_fit, rfit_min, rfit_max):
        
        self.p_bias = np.array([2,1,1])# get from wgg fit
        self.z_fit = z_fit

        self.cosmo = ccl.Cosmology(Omega_c=0.25, Omega_b=0.044, h=0.7, sigma8=0.8, n_s=0.95)
        
        root = './'
        
        self.dir_chains = root + 'chains/'
        
        self.mcmc_code = root + 'run_mcmc.py'

        self.suffix = suffix
        
        self.fname_chain_linbias = self.dir_chains + 'chain_linbias_' + suffix + '.h5'
        self.fname_chain_quadbias = self.dir_chains + 'chain_quadbias_' + suffix + '.h5'
        self.fname_chain_nla = self.dir_chains + 'chain_nla_' + suffix + '.h5'
        self.fname_chain_tatt = self.dir_chains + 'chain_tatt_' + suffix + '.h5'

        self.chain_linbias = np.array
        self.chain_quadbias = np.array
        self.chain_nla = np.array
        self.chain_tatt = np.array

        self.lgP_linbias = np.array
        self.lgP_quadbias = np.array
        self.lgP_nla = np.array
        self.lgP_tatt = np.array
        
        self.fname_wgg = fname_wgg
        self.fname_cov_wgg = fname_cov_wgg
        self.wgg = pd.DataFrame()
        self.wgg_rcut = pd.DataFrame()
        self.dim_wgg_rcut = 99

        self.fname_wgp = fname_wgp
        self.fname_cov_wgp = fname_cov_wgp
        self.wgp = pd.DataFrame()
        self.wgp_rcut = pd.DataFrame()
        self.dim_wgp_rcut = 99
        
        self.rfit_min, self.rfit_max = rfit_min, rfit_max

        self.p_bias = np.asarray([2,1,1])

        self.ndim_linbias_model = 1
        self.ndim_quadbias_model = 3
        self.ndim_nla_model = 1
        self.ndim_tatt_model = 3

        self.p_linbias_margi = np.full((self.ndim_linbias_model,3),np.nan)
        self.p_quadbias_margi = np.full((self.ndim_quadbias_model,3),np.nan)
        self.p_nla_margi = np.full((self.ndim_nla_model,3),np.nan)
        self.p_tatt_margi = np.full((self.ndim_tatt_model,3),np.nan)

        self.p_linbias_maxL= np.full(self.ndim_linbias_model,np.nan)
        self.p_quadbias_maxL= np.full(self.ndim_quadbias_model,np.nan)
        self.p_nla_maxL = np.full(self.ndim_nla_model,np.nan)
        self.p_tatt_maxL = np.full(self.ndim_tatt_model,np.nan)

        self.chisq_min_dof_linbias = 999.
        self.chisq_min_dof_quadbias = 999.
        self.chisq_min_dof_nla = 999.
        self.chisq_min_dof_tatt = 999.
        
        self.r_model = np.array
        self.wgg_linbias = np.array
        self.wgp_quadbias = np.array
        self.wgp_nla = np.array
        self.wgp_tatt = np.array
        
        self.wmm_nl = np.array
        self.wmm_nl_int = np.array
        
    def read_wgg(self):
        
        self.wgg = pd.read_csv(self.fname_wgg)
        self.wgg.dropna(inplace=True)
        self.wgg_rcut = self.wgg[(self.rfit_min <= self.wgg.rp) & (self.wgg.rp <= self.rfit_max)]
        self.dim_wgg_rcut = len(self.wgg_rcut)
        
    def read_wgp(self):
        
        self.wgp = pd.read_csv(self.fname_wgp)
        self.wgp.dropna(inplace=True)
        self.wgp_rcut = self.wgp[(self.rfit_min <= self.wgp.rp) & (self.wgp.rp <= self.rfit_max)]
        self.dim_wgp_rcut = len(self.wgp_rcut)

        
    def _get_chain(self, nstep_burn, thin, fname_chain):
        reader = emcee.backends.HDFBackend(fname_chain)
        return reader.get_chain(discard=nstep_burn, thin=thin, flat=True)

    def _get_log_prob(self, nstep_burn, thin, fname_chain):
        reader = emcee.backends.HDFBackend(fname_chain)
        return reader.get_log_prob(discard=nstep_burn, thin=thin, flat=True)
        
        
    def _find_nearest(self,X, value):
        return X[np.unravel_index(np.argmin(np.abs(X - value)), X.shape)]

    
    def _get_params_fit(self, chain, lgP, dof):
        
        params_fit = chain[lgP == lgP.max(),:][0]

        # corresponding chisq/dof
        chisq_dof = -2*lgP.max()/float(dof)
    
        return params_fit, chisq_dof
    
    def _get_params_fit_margi(self, chain, conf_lev):

        Nbin = 100000

        ndim = chain.shape[1]
        params_fit_margi = np.full((ndim,3),np.nan)

        cfrac = 0.5*(100-conf_lev)/100.

        #maxima and confidence levels of marginalized posterior
        for i in range(ndim):

            x = np.linspace(chain[:,i].min(), chain[:,i].max(),Nbin)
            kernel = stats.gaussian_kde(chain[:,i])
            chain_smooth = kernel(x)

            #sum_list = np.cumsum(hist)/sum(hist)
            sum_pdf = np.cumsum(chain_smooth) / sum(chain_smooth)

            # max marginalized likelihood
            params_fit_margi[i,0] = x[np.where(chain_smooth == np.amax(chain_smooth))][0]

            # confidence intervals
            params_fit_margi[i,1] = x[np.where(sum_pdf == self._find_nearest(sum_pdf, cfrac))][0]
            params_fit_margi[i,2] = x[np.where(sum_pdf == self._find_nearest(sum_pdf, 1-cfrac))][0]

        return params_fit_margi
        
    def run_mcmc_wgg(self, reset_chain, nstep, nstep_burn, nstep_thin, nwalkers, ncore, rfit_min, rfit_max, bias_model, p_bias_ini):
        
        self.rfit_min, self.rfit_max = rfit_min, rfit_max
        
        if bias_model == 'linbias':
            
            b1_ini = p_bias_ini
            b2_ini = 0.0
            bs_ini = 0.0
            
            call(['python3', self.mcmc_code, 'wgg', bias_model, self.fname_wgg, self.fname_cov_wgg, self.fname_chain_linbias, str(self.z_fit), str(nstep), str(nwalkers), str(ncore), reset_chain, str(rfit_min), str(rfit_max), str(b1_ini), str(b2_ini), str(bs_ini)])

            self.chain_linbias = self._get_chain(nstep_burn, nstep_thin, self.fname_chain_linbias)
            self.lgP_linbias = self._get_log_prob(nstep_burn, nstep_thin, self.fname_chain_linbias)

            self.p_linbias_margi = self._get_params_fit_margi(self.chain_linbias, 68)
            self.p_linbias_maxL, self.chisq_min_dof_linbias = self._get_params_fit(self.chain_linbias, self.lgP_linbias, self.dim_wgg_rcut)

        if bias_model == 'quadbias':
            
            b1_ini, b2_ini, bs_ini = p_bias_ini
            
            call(['python3', self.mcmc_code, 'wgg', bias_model, self.fname_wgg, self.fname_cov_wgg, self.fname_chain_quadbias, str(self.z_fit), str(nstep), str(nwalkers), str(ncore), reset_chain, str(rfit_min), str(rfit_max), str(b1_ini), str(b2_ini), str(bs_ini)])

            self.chain_quadbias = self._get_chain(nstep_burn, nstep_thin, self.fname_chain_quadbias)
            self.lgP_quadbias = self._get_log_prob(nstep_burn, nstep_thin, self.fname_chain_quadbias)

            self.p_quadbias_margi = self._get_params_fit_margi(self.chain_quadbias, 68)
            self.p_quadbias_maxL, self.chisq_min_dof_quadbias = self._get_params_fit(self.chain_quadbias, self.lgP_quadbias, self.dim_wgg_rcut)

    
    def run_mcmc_wgp(self, reset_chain, nstep, nstep_burn, nstep_thin, nwalkers, ncore, rfit_min, rfit_max, ia_model, p_ia_ini, p_bias):
        
        self.rfit_min, self.rfit_max = rfit_min, rfit_max
            
        #b1, b2, bs = self.p_linbias_maxL[0],0,0
        b1, b2, bs = p_bias
        

        if ia_model == 'nla':
            
            fname_chain = self.fname_chain_nla
            
            a1_ini = p_ia_ini
            a2_ini = 0.0
            as_ini = 0.0
            
            call(['python3', self.mcmc_code, 'wgp', ia_model, self.fname_wgp, self.fname_cov_wgp, self.fname_chain_nla, str(self.z_fit), str(nstep), str(nwalkers), str(ncore), reset_chain, str(rfit_min), str(rfit_max), str(a1_ini), '0.0', '0.0', str(b1), str(b2), str(bs)])
            
            self.chain_nla = self._get_chain(nstep_burn, nstep_thin, self.fname_chain_nla)
            self.lgP_nla = self._get_log_prob(nstep_burn, nstep_thin, self.fname_chain_nla)
            
            self.p_nla_margi = self._get_params_fit_margi(self.chain_nla, 68)
            self.p_nla_maxL, self.chisq_min_dof_nla = self._get_params_fit(self.chain_nla, self.lgP_nla, self.dim_wgp_rcut)

        if ia_model == 'tatt':
            
            fname_chain = self.fname_chain_tatt
            
            a1_ini, a2_ini, ad_ini = p_ia_ini

            call(['python3', self.mcmc_code, 'wgp', ia_model, self.fname_wgp, self.fname_cov_wgp, self.fname_chain_tatt, str(self.z_fit), str(nstep), str(nwalkers), str(ncore), reset_chain, str(rfit_min), str(rfit_max), str(a1_ini), str(a2_ini), str(ad_ini), str(b1), str(b2), str(bs)])
            
            self.chain_tatt = self._get_chain(nstep_burn, nstep_thin, self.fname_chain_tatt)
            self.lgP_tatt = self._get_log_prob(nstep_burn, nstep_thin, self.fname_chain_tatt)
            
            self.p_tatt_margi = self._get_params_fit_margi(self.chain_tatt, 68)
            self.p_tatt_maxL, self.chisq_min_dof_tatt = self._get_params_fit(self.chain_tatt, self.lgP_tatt, self.dim_wgp_rcut)

            
    def get_wmm_nl(self):
    
        nk = 1024
        log10kmin, log10kmax = -5, 3
        ks = np.logspace(log10kmin,log10kmax,nk)
        k = ks/self.cosmo['h'] # convert to h/Mpc

        ptc = pt.PTCalculator(with_NC=True, with_IA=False, log10k_min=log10kmin, log10k_max=log10kmax, nk_per_decade=20)
        
        ptt_m = pt.PTMatterTracer()

        # matter x matter
        pk_mm = pt.get_pt_pk2d(self.cosmo, ptt_m, tracer2=ptt_m, ptc=ptc)

        #power spectrum
        p_mm = self.cosmo['h']**3*pk_mm.eval(ks,1./(1+self.z_fit),self.cosmo)

        #projected correlations from Hankel Transform
        self.r_model, self.wmm_nl = HT.k_to_r(k,p_mm,1.,-1.,0., 1./(2*np.pi))

    def get_wgg_model(self, bias_model):
    
        nk = 1024
        log10kmin, log10kmax = -5, 3
        ks = np.logspace(log10kmin,log10kmax,nk)
        k = ks/self.cosmo['h'] # convert to h/Mpc

        if bias_model=='linbias':  b1, b2, bs = self.p_linbias_maxL[0],0,0
        if bias_model=='quadbias': b1, b2, bs = self.p_quadbias_maxL
        
        # galaxy x galaxy
        ptc = pt.PTCalculator(with_NC=True, with_IA=False, log10k_min=log10kmin, log10k_max=log10kmax, nk_per_decade=20)
        ptt_g = pt.PTNumberCountsTracer(b1=b1, b2=b2, bs=bs)
        
        #power spectra
        pk_gg = pt.get_pt_pk2d(self.cosmo, ptt_g, ptc=ptc)
        p_gg = self.cosmo['h']**3*pk_gg.eval(ks,1./(1+self.z_fit),self.cosmo)

        #projected correlations from Hankel Transform
        if bias_model=='linbias':  self.r_model, self.wgg_linbias = HT.k_to_r(k,p_gg,1.,-1.,0., 1./(2*np.pi))
        if bias_model=='quadbias': self.r_model, self.wgg_quadbias = HT.k_to_r(k,p_gg,1.,-1.,0., 1./(2*np.pi))
        

    def get_wgp_model(self, ia_model):

        z = np.linspace(0, 2.5, 1024)

        b1, b2, bs = self.p_linbias_maxL[0],0,0
        
        if ia_model == 'nla': a1, a2, ad = self.p_nla_maxL[0],0,0
        if ia_model == 'tatt': a1, a2, ad = self.p_tatt_maxL

        nk = 1024
        log10kmin, log10kmax = -5, 3
        ks = np.logspace(log10kmin,log10kmax,nk)
        k = ks/self.cosmo['h'] # convert to h/Mpc

        #c_1 = pt.translate_IA_norm(cosmo, z, a1=a_1, Om_m2_for_c2 = False)[0]#NLA
        c_1,c_d,c_2 = pt.translate_IA_norm(self.cosmo, z, a1=a1, a1delta=ad, a2=a2, Om_m2_for_c2 = False)

        ptc = pt.PTCalculator(with_NC=True, with_IA=True, log10k_min=log10kmin, log10k_max=log10kmax, nk_per_decade=20)

        ptt_g = pt.PTNumberCountsTracer(b1=b1, b2=b2, bs=bs)
        ptt_i = pt.PTIntrinsicAlignmentTracer(c1=(z,c_1), c2=(z,c_2), cdelta=(z,c_d))

        #power spectra    
        pk_gi = pt.get_pt_pk2d(self.cosmo, ptt_g, tracer2=ptt_i, ptc=ptc)
        p_gI = self.cosmo['h']**3*pk_gi.eval(ks,1./(1+self.z_fit),self.cosmo)

        #projected correlations from Hankel Transform        
        if ia_model == 'nla': self.r_model, self.wgp_nla = HT.k_to_r(k,p_gI,1.,-1.,2., -1./(2*np.pi))
        if ia_model == 'tatt': self.r_model, self.wgp_tatt = HT.k_to_r(k,p_gI,1.,-1.,2., -1./(2*np.pi))

    def interpolate_models(self):
        
        self.wmm_nl_int = np.interp(self.corr.rp, self.r_model, self.wmm_nl)

        self._linbias_int = np.interp(self.corr.rp, self.r_model, self._linbias)
        self._quadbias_int = np.interp(self.corr.rp, self.r_model, self._quadbias)

        self.wgp_nla_int = np.interp(self.corr.rp, self.r_model, self.wgp_nla)
        self.wgp_tatt_int = np.interp(self.corr.rp, self.r_model, self.wgp_tatt)
