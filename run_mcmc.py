from matplotlib import pyplot as plt
import numpy as np
import fastpt as fpt
import fastpt.HT as HT
import pyccl as ccl
import pyccl.ccllib as lib
import pyccl.nl_pt as pt
import pandas as pd
import json
import emcee
from multiprocessing import Pool
import sys
import os

#input parameters
var = sys.argv[1]
if var=='wgg':bias_model = sys.argv[2]
if var=='wgp':ia_model = sys.argv[2]
fname_corr = sys.argv[3]
fname_cov = sys.argv[4]
fname_chain = sys.argv[5]
zz_glob = float(sys.argv[6])
nstep = float(sys.argv[7])
nwalkers = int(sys.argv[8])
ncore = int(sys.argv[9])
reset_chain = (sys.argv[10]=='True')
rfit_min, rfit_max = float(sys.argv[11]), float(sys.argv[12])

if var=='wgg':
    b1_ini, b2_ini, bs_ini = float(sys.argv[13]), float(sys.argv[14]), float(sys.argv[15])

if var=='wgp':
    
    a1_ini, a2_ini, ad_ini = float(sys.argv[13]), float(sys.argv[14]), float(sys.argv[15])

    b1, b2, bs = float(sys.argv[16]), float(sys.argv[17]), float(sys.argv[18])



#========= cosmology =========
p_cosmo = np.asarray([0.25, 0.044, 0.7, 0.8, 0.95])


#====== read correlation measurements =========
corr = pd.read_csv(fname_corr)
corr.dropna(inplace=True)

select = (rfit_min <= corr.rp) & (corr.rp <= rfit_max)

corr_rcut = corr[select]


#====== read covariance =========
with open(fname_cov, "r") as fp: Cin_wgp = json.load(fp)

C = np.array(Cin_wgp)

Cnorm = np.zeros((C.shape[0], C.shape[1]))

for i in range(C.shape[0]):
    for j in range(C.shape[1]):
        Cnorm[i,j] = C[i,j]/(C.diagonal()[i]*C.diagonal()[j])**0.5

#apply scale cut on matrix
idx = np.arange(0,len(corr))

cut_lo, cut_hi = idx[select].min(), idx[select].max()
Cnorm_cut = Cnorm[cut_lo:cut_hi+1, cut_lo: cut_hi+1]

Cnorm_cut_inv = np.linalg.inv(Cnorm_cut)



#======== global variables =========
cosmo_glob = ccl.Cosmology(Omega_c=p_cosmo[0], Omega_b=p_cosmo[1], h=p_cosmo[2], sigma8=p_cosmo[3], n_s=p_cosmo[4])

data_glob = corr_rcut

ptc_glob = pt.PTCalculator(with_NC=True, with_IA=True, log10k_min=-5, log10k_max=2, nk_per_decade=20)


# ======== functions ========

#---------------------------------------------------------
def get_wgg(cosmo, zz, ptc, p_bias):
    
    b1, b2, bs = p_bias

    nk = 512
    log10kmin, log10kmax = -5, 2
    ks = np.logspace(log10kmin,log10kmax,nk)
    k = ks/cosmo['h'] # convert to h/Mpc
    
    ptt_g = pt.PTNumberCountsTracer(b1=b1, b2=b2, bs=bs)
    
    #power spectra
    pk_gg = pt.get_pt_pk2d(cosmo, ptt_g, ptc=ptc)
    p_gg = cosmo['h']**3*pk_gg.eval(ks,1./(1+zz),cosmo)
    
    #projected correlations from Hankel Transform
    r,wgg = HT.k_to_r(k,p_gg,1.,-1.,0., 1./(2*np.pi))
    
    return r, wgg

#---------------------------------------------------------
def get_wgp(cosmo, zz, ptc, p_bias, p_IA):
    
    z = np.linspace(0, 1.5, 1024)

    b1, b2, bs = p_bias
    a1, a2, ad = p_IA

    nk = 512
    log10kmin, log10kmax = -5, 2
    ks = np.logspace(log10kmin,log10kmax,nk)
    k = ks/cosmo['h'] # convert to h/Mpc
    
    #c_1 = pt.translate_IA_norm(cosmo, z, a1=a_1, Om_m2_for_c2 = False)[0]#NLA
    c_1,c_d,c_2 = pt.translate_IA_norm(cosmo, z, a1=a1, a1delta=ad, a2=a2, Om_m2_for_c2 = False)
    
    ptt_g = pt.PTNumberCountsTracer(b1=b1, b2=b2, bs=bs)
    ptt_i = pt.PTIntrinsicAlignmentTracer(c1=(z,c_1), c2=(z,c_2), cdelta=(z,c_d))

    #power spectra    
    pk_gi = pt.get_pt_pk2d(cosmo, ptt_g, tracer2=ptt_i, ptc=ptc)
    p_gI = cosmo['h']**3*pk_gi.eval(ks,1./(1+zz),cosmo)
    
    #projected correlations from Hankel Transform
    r,wgp = HT.k_to_r(k,p_gI,1.,-1.,2., -1./(2*np.pi))
    
    return r, wgp


#---------------------------------------------------------
def log_likelihood_wgg(params_bias, bias_model):
    
    if bias_model=='linbias': params = [params_bias[0],0,0]
    if bias_model=='quadbias': params = params_bias
    
    rp_model, wgg_model = get_wgg(cosmo_glob, zz_glob, ptc_glob, params)
    
    wgg_model_interpol = np.interp(data_glob.rp, rp_model, wgg_model)

    delta_wgg = (data_glob.wgg - wgg_model_interpol)/data_glob.wggerr
    
    chisq = delta_wgg.dot(Cnorm_cut_inv.dot(delta_wgg.T))
    ll = -0.5 * chisq
    
    #ll = -0.5 * np.dot(delta_wgg, delta_wgg)
        
    if np.isnan(ll):
        return -np.inf
    return ll



#---------------------------------------------------------
def log_likelihood_wgp(params_bias, params_IA, ia_model):

    if ia_model=='nla': params = [params_IA[0],0,0]
    if ia_model=='tatt': params = params_IA

    rp_model, wgp_model = get_wgp(cosmo_glob, zz_glob, ptc_glob, params_bias, params)
    
    wgp_model_interpol = np.interp(data_glob.rp, rp_model, wgp_model)

    delta_wgp = (data_glob.wgp - wgp_model_interpol)/data_glob.wgperr
    
    chisq = delta_wgp.dot(Cnorm_cut_inv.dot(delta_wgp.T))
    ll = -0.5 * chisq
    
    #ll = -0.5 * np.dot(delta_wgp, delta_wgp)#without covariance
        
    if np.isnan(ll):
        return -np.inf
    return ll

#---------------------------------------------------------
def log_prior_linbias(p):
    if 0 < p[0] < 10:
        return 0.0
    return -np.inf

#---------------------------------------------------------
def log_prior_quadbias(p):
    if 0 < p[0] < 10 and -10 < p[1] < 10 and -10 < p[2] < 10:
        return 0.0
    return -np.inf


#---------------------------------------------------------
def log_prior_nla(p):
    if -25 < p < 25:
        return 0.0
    return -np.inf

#---------------------------------------------------------
def log_prior_tatt(p):
    if -25 < p[0] < 25 and -25 < p[1] < 25 and -25 < p[2] < 25:
        return 0.0
    return -np.inf


#---------------------------------------------------------
def log_probability_wgg_linbias(params_bias):

    lp = log_prior_linbias(params_bias)
    
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood_wgg(params_bias, 'linbias')


#---------------------------------------------------------
def log_probability_wgg_quadbias(params_bias):

    lp = log_prior_quadbias(params_bias)
    
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood_wgg(params_bias, 'quadbias')


#---------------------------------------------------------
def log_probability_wgp_nla(params_IA, b1, b2, bs):

    params_bias = np.array([b1,b2,bs])

    lp = log_prior_nla(params_IA)
    
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood_wgp(params_bias, params_IA, 'nla')


#---------------------------------------------------------
def log_probability_wgp_tatt(params_IA, b1, b2, bs):

    params_bias = np.array([b1,b2,bs])

    lp = log_prior_tatt(params_IA)
    
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood_wgp(params_bias, params_IA, 'tatt')


#---------------------------------------------------------
def run_mcmc_wgg(reset_chain, nwalkers, ncore, nstep, fname_chain, bias_model, p_bias_ini):

    ndim = len(p_bias_ini)


    if bias_model=='linbias':
                
        backend = emcee.backends.HDFBackend(fname_chain)
        if(reset_chain):backend.reset(nwalkers, ndim)
        
        initial = p_bias_ini[0] + 0.1 * np.random.randn(nwalkers, ndim)
        

        with Pool(ncore) as pool:
            sampler = emcee.EnsembleSampler(
                nwalkers, 
                ndim,
                log_probability_wgg_linbias,
                moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)],
                pool=pool,
                backend=backend
            )
            sampler.run_mcmc(initial, nstep, progress=True, store=True)

        
        
    if bias_model=='quadbias':
                
        backend = emcee.backends.HDFBackend(fname_chain)
        if(reset_chain):backend.reset(nwalkers, ndim)
        
        initial = p_bias_ini + 0.1 * np.random.randn(nwalkers, ndim)
        

        with Pool(ncore) as pool:
            sampler = emcee.EnsembleSampler(
                nwalkers, 
                ndim,
                log_probability_wgg_quadbias,
                moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)],
                pool=pool,
                backend=backend
            )
            sampler.run_mcmc(initial, nstep, progress=True, store=True)
    


                
#---------------------------------------------------------
def run_mcmc_wgp(reset_chain, nwalkers, ncore, nstep, fname_chain, ia_model, p_ia_ini, p_bias):
    
    ndim = len(p_ia_ini)

    if ia_model=='nla':
                
        backend = emcee.backends.HDFBackend(fname_chain)
        if(reset_chain):backend.reset(nwalkers, ndim)
        
        initial = p_ia_ini[0] + 0.1 * np.random.randn(nwalkers, ndim)
        
        with Pool(ncore) as pool:
            sampler = emcee.EnsembleSampler(
                nwalkers, 
                ndim,
                log_probability_wgp_nla,
                args=(p_bias[0], p_bias[1], p_bias[2]),
                moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)],
                pool=pool,
                backend=backend
            )
            sampler.run_mcmc(initial, nstep, progress=True, store=True)
    
    
    
    
    if ia_model=='tatt':
                
        backend = emcee.backends.HDFBackend(fname_chain)
        if(reset_chain):backend.reset(nwalkers, ndim)
        
        initial = p_ia_ini + 0.1 * np.random.randn(nwalkers, ndim)
        
        with Pool(ncore) as pool:
            sampler = emcee.EnsembleSampler(
                nwalkers, 
                ndim,
                log_probability_wgp_tatt,
                args=(p_bias[0], p_bias[1], p_bias[2]),
                moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)],
                pool=pool,
                backend=backend
            )
            sampler.run_mcmc(initial, nstep, progress=True, store=True)
    
            
#========= run mcmc =========
#projected galaxy-galaxy correlation
if var=='wgg':
    
    if(bias_model=='linbias'):
        p_bias_ini = np.array([b1_ini])
        
    if(bias_model=='quadbias'):
        p_bias_ini = np.array([b1_ini, b2_ini, bs_ini])

    
    run_mcmc_wgg(reset_chain, nwalkers, ncore, nstep, fname_chain, bias_model, p_bias_ini)


#projected galaxy-shear correlation
if var=='wgp':

    p_bias = np.array([b1, b2, bs])
    
    if(ia_model=='nla'): 
        p_ia_ini = np.array([a1_ini])
    
    if(ia_model=='tatt'): 
        p_ia_ini = np.array([a1_ini, a2_ini, ad_ini])

    run_mcmc_wgp(reset_chain, nwalkers, ncore, nstep, fname_chain, ia_model, p_ia_ini, p_bias)
    
